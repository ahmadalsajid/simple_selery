from celery import Celery
from celery.schedules import crontab
import MySQLdb
import random
import string
import time
import datetime

app = Celery('db_update', broker="pyamqp://guest@localhost//")
# disable UTC to use local time
app.conf.enable_utc = False

warehouse_db = MySQLdb.connect(user='root', passwd="qweqwe", db="warehouse")
warehouse_cursor = warehouse_db.cursor()

ific_db = MySQLdb.connect(user='root', passwd="qweqwe", db="ific")
ific_cursor = ific_db.cursor()


def execute_query(query):
    try:
        warehouse_cursor.execute(query)
        rows = warehouse_cursor.fetchall()
        # print(rows)
        return rows
    except Exception as e:
        raise


def generate_query(tdate):
    search_month = datetime.datetime.strptime(tdate, '%Y-%m-%d').date()

    # total_std (1010267)
    coa_1010267 = "SELECT branch_code, '1010267', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('100') GROUP BY branch_code"

    # stationary & stamp (1010223)
    coa_1010223 = "SELECT branch_code, '1010223', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('096','098') GROUP BY branch_code"

    # Other Receivables(1010224)
    coa_1010224 = "SELECT branch_code, '1010224', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('108') GROUP BY branch_code"

    # Head Office General Ledger Positive Balance (after netting off) (1010208)
    coa_1010208 = "SELECT branch_code, '1010208', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('090','210') GROUP BY branch_code"

    # Total Deposit (1010215)
    coa_1010215 = "SELECT branch_code, '1010215', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno BETWEEN '147' AND '188' GROUP BY branch_code"

    # Total Savings Deposit (1010266)
    coa_1010266 = "SELECT branch_code, '1010266', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('150','151','153','162','162','164','167') GROUP BY branch_code"

    # Total Term Deposit (1010268)
    coa_1010268 = "SELECT branch_code, '1010268', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('154') GROUP BY branch_code"

    # Total Savings Scheme Deposit (1010269)
    coa_1010269 = "SELECT branch_code, '1010269', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('156','159','169','171','173','175','155','157','166','176','177','178') GROUP BY branch_code"

    # Local DD, TT, MT & PO Payable (1010280)
    coa_1010280 = "SELECT branch_code, '1010280', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno BETWEEN '180' AND '188' GROUP BY branch_code"

    # Sundry Creditors (1010288)
    coa_1010288 = "SELECT branch_code, '1010288', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('161') GROUP BY branch_code"

    # Head Office General Ledger Negative Balance (after netting off) (1010235)
    coa_1010235 = "SELECT branch_code, '1010235', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('090','210') GROUP BY branch_code"

    # Total Off Balance Sheet Exposure (1010291)
    coa_1010291 = "SELECT branch_code, '1010291', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno BETWEEN '212' AND '240' GROUP BY branch_code"

    # Acceptance and Endorsement (Affairs IFDBR) (1010292)
    coa_1010292 = "SELECT branch_code, '1010292', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('230') GROUP BY branch_code"

    # Letters of Guarantee (1010293)
    coa_1010293 = "SELECT branch_code, '1010293', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('218') GROUP BY branch_code"

    # Irrevocable Letters of Credit (1010294)
    coa_1010294 = "SELECT branch_code, '1010294', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('220','222','224','226') GROUP BY branch_code"

    # Bills for Collection (1010295)
    coa_1010295 = "SELECT branch_code, '1010295', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN ('212','214','216','238') GROUP BY branch_code"

    # Total Loan Outstanding (1010405)
    coa_1010405 = "SELECT branch_code, '1010405', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno BETWEEN '040' AND '089' GROUP BY branch_code"

    # Total Interest Suspense Against Loan (1010450)
    coa_1010450 = "SELECT branch_code, '1010450', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('209') GROUP BY branch_code"

    # Total Interest Suspense Balance (1010540)
    coa_1010540 = "SELECT branch_code, '1010540', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('209') GROUP BY branch_code"

    # Total Cash Credit Hypo Loan / Cash Credit / '058' COACT=0181  && Total Cash Credit Pledge Loan will be blank COACT=0182  (1010475)
    coa_1010475 = "SELECT branch_code, '1010475', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('058') GROUP BY branch_code"

    # Total ECC (1010510)
    coa_1010510 = "SELECT branch_code, '1010510', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('082') GROUP BY branch_code"

    # Total PAD (General) (1010500)
    coa_1010500 = "SELECT branch_code, '1010500', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('069','071','074','075') GROUP BY branch_code"

    # Total PAD(EDF) (1010615)
    coa_1010615 = "SELECT branch_code, '1010615', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('073') GROUP BY branch_code"

    # Total LTR/MPI (1010490)
    coa_1010490 = "SELECT branch_code, '1010490', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('081') GROUP BY branch_code"

    # Total LIM (1010495)
    coa_1010495 = "SELECT branch_code, '1010495', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('079') GROUP BY branch_code"

    # Total Loan Outstanding Against IBP/LDBP (1010530)
    coa_1010530 = "SELECT branch_code, '1010530', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('067') GROUP BY branch_code"

    # Total Loan Outstanding against FDBP (1010535)
    coa_1010535 = "SELECT branch_code, '1010535', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('065') GROUP BY branch_code"

    # Total Lease Financing (1010515)
    coa_1010515 = "SELECT branch_code, '1010515', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('087') GROUP BY branch_code"

    # Total Loan Against Credit Cards (1010517)
    coa_1010517 = "SELECT branch_code, '1010517', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('077') GROUP BY branch_code"

    # Total Outstanding Balance of Issued Bank Guarantee (1010750)
    coa_1010750 = "SELECT branch_code, '1010750', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('117') GROUP BY branch_code"

    # Total Foreign Currency in Hand (1010830)
    coa_1010830 = "SELECT branch_code, '1010830', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('012') GROUP BY branch_code"

    # Total Foreign Exchange Holding (1010825)
    coa_1010825 = "SELECT branch_code, '1010825', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('012','024') GROUP BY branch_code"

    # Total Customer Balance in the Foreign Currency A/C (1010865)
    coa_1010865 = "SELECT branch_code, '1010865', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('162','163','164','165','166','167') GROUP BY branch_code"

    # Cash in Vault (1011625)
    coa_1011625 = "SELECT branch_code, '1011625', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('010','011','012','036') GROUP BY branch_code"

    # Local Currency in Vault (1011657)
    coa_1011657 = "SELECT branch_code, '1011657', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('010','011') GROUP BY branch_code"

    # Foreign Currency in Vault (1011659)
    coa_1011659 = "SELECT branch_code, '1011659', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('012') GROUP BY branch_code"

    #  Prize Bond in Vault (1011661)
    coa_1011661 = "SELECT branch_code, '1011661', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('036') GROUP BY branch_code"

    # Cash Balance in ATM (1011663)
    coa_1011663 = "SELECT branch_code, '1011663', ABS(SUM(balance)) FROM glbalance WHERE MONTH(tdate)= " + str(
        search_month.month) + "  AND YEAR(tdate)= " + str(
        search_month.year) + " AND accountno IN('013') GROUP BY branch_code"

    query_list = [coa_1010267, coa_1010223, coa_1010224, coa_1010208, coa_1010215, coa_1010266,
                  coa_1010268, coa_1010269, coa_1010280, coa_1010288, coa_1010235, coa_1010291,
                  coa_1010292, coa_1010293, coa_1010294, coa_1010295, coa_1010405, coa_1010450,
                  coa_1010540, coa_1010475, coa_1010510, coa_1010500, coa_1010615, coa_1010490,
                  coa_1010495, coa_1010530, coa_1010535, coa_1010515, coa_1010517, coa_1010750,
                  coa_1010830, coa_1010825, coa_1010865, coa_1011625, coa_1011657, coa_1011659,
                  coa_1011661, coa_1011663]

    # query_list = [coa_1010267]

    try:
        coa_data = []
        for query in query_list:
            coa_data += execute_query(query)
    except Exception as e:
        raise

    return coa_data

@app.task
def pull_data():
    start = time.time()
    query_date = '2019-03-28'

    working_month = datetime.datetime.strptime(query_date, '%Y-%m-%d').date().replace(day=1)
    print(working_month, type(working_month))

    data = generate_query(query_date)

    insertion_data = []
    for _ in data:
        tmp = list(_)
        tmp.append(working_month.strftime('%Y-%m-%d'))
        insertion_data.append(tmp)

    # for _ in insertion_data:
    #     print(_)
    truncate_query = """TRUNCATE TABLE `monthly_iss_data`;"""

    ific_q = """INSERT INTO `monthly_iss_data` (`branch_code`, `coa_id`, `amount`, `month`, `source_type`) VALUES (%s, %s, %s, %s, 1)"""

    # truncate table to clear previous data
    ific_cursor.execute(truncate_query)
    ific_db.commit()

    ific_cursor.executemany(ific_q, insertion_data)
    ific_db.commit()

    print('time taken: {:.2f} seconds.'.format(time.time()-start))


# add "update_data" task to the beat schedule
app.conf.beat_schedule = {
    "pull-data": {
        "task": "pull_from_warehouse.pull_data",
        "schedule": crontab(minute='*'),
    }
}

# if __name__ == '__main__':
#     pull_data()

